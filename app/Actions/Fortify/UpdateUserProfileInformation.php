<?php

namespace App\Actions\Fortify;

use App\Models\UserDetail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param mixed $user
     * @param array $input
     * @return void
     */
    public function update($user, array $input)
    {
        Validator::make($input, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'address' => ['nullable', 'string', 'max:512'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            'photo' => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
        ])->validateWithBag('updateProfileInformation');

        DB::transaction(function () use ($input, $user) {
            if (isset($input['photo'])) {
                $user->updateProfilePhoto($input['photo']);
            }

            if ($input['email'] !== $user->email &&
                $user instanceof MustVerifyEmail) {
                $this->updateVerifiedUser($user, $input);
            } else {
                $user->forceFill([
                    'first_name' => $input['first_name'],
                    'last_name' => $input['last_name'],
                    'email' => $input['email'],
                ])->save();

                //constrained to single result, but can be switched to many details if required
                $details = $user->userDetails()->first() ?? new UserDetail();
                $details->fill([
                    'user_id' => $user->id,
                    'address' => $input['address'] ?? '',
                    'country' => $input['country'] ?? '',
                ])->save();
            }
        });
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param mixed $user
     * @param array $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        DB::transaction(function () use ($input, $user) {
            $user->forceFill([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'email_verified_at' => null,
            ])->save();

            //constrained to single result, but can be switched to many details if required
            $details = $user->userDetails()->first() ?? new UserDetail();
            $details->fill([
                'user_id' => $user->id,
                'address' => $input['address'],
                'country' => $input['country'],
            ])->save();
        });

//        $user->sendEmailVerificationNotification();
    }
}
