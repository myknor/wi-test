<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateApiUsersRequest;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\PersonalAccessToken;


class ApiUserController extends Controller
{
    /**
     * @var PersonalAccessToken|null
     */
    private PersonalAccessToken $token;

    /**
     * ApiUserController constructor.
     */
    public function __construct()
    {
        $user = Auth::guard('sanctum')->user();
        $this->token = $user ? $user->currentAccessToken() : new PersonalAccessToken();
    }

    /**
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function index()
    {
        if (!$this->token->can('users:read')) {
            return response()->json(["message" => "Forbidden."], 403);
        }

        return response(User::with('userDetails')->get());
    }

    /**
     * @param User $user
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function show(User $user)
    {
        if (!$this->token->can('users:read')) {
            return response()->json(["message" => "Forbidden."], 403);
        }

        return response($user->load('userDetails'));
    }

    /**
     * @param UpdateApiUsersRequest $request
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function store(UpdateApiUsersRequest $request)
    {
        if (!$this->token->can('users:create')) {
            return response()->json(["message" => "Forbidden."], 403);
        }

        return $this->update($request, new User());
    }

    /**
     * @param UpdateApiUsersRequest $request
     * @param User $user
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function update(UpdateApiUsersRequest $request, User $user)
    {
        if ($user->exists && !$this->token->can('users:update')) {
            return response()->json(["message" => "Forbidden."], 403);
        }

        DB::transaction(function () use ($request, $user) {
            $user->fill($request->all())->save();
            //constrained to single result, but can be switched to many details if required
            $details = $user->userDetails()->first() ?? new UserDetail();
            $details->fill([
                'user_id'=>$user->id,
                'address' => $request->address ?? '',
                'country' => $request->country ?? '',
            ])->save();
        });

        return response(['status' => 'ok', 'user' => $user,]);
    }

    /**
     * @param User $user
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function destroy(User $user)
    {
        if ($user->exists && !$this->token->can('users:delete')) {
            return response()->json(["message" => "Forbidden."], 403);
        }

        $user->delete();
        return response(['status' => 'ok',]);
    }
}
