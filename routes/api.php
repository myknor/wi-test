<?php

use App\Http\Controllers\Api\ApiUserController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('/')->name('api.')->middleware(['auth:sanctum'])->group( function () {
    Route::resource('users', ApiUserController::class);
    Route::get('inertia', function () {
        return Inertia::render('UserListFromApi');
    })->name('UserListFromApi');
});

